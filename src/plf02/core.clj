(ns plf02.core)

(defn función-associative?-1
  [c]
  (associative? c))
(defn función-associative?-2
  [c]
  (associative? c))
(defn función-associative?-3
  [c]
  (associative? c))
(función-associative?-1 [5 9 6 0])
(función-associative?-2 '([54 9 51 20] [87 69 5 0]))
(función-associative?-3 {:17161210 "Oscar" :1716120 "Erik"})

(defn función-boolean?-1
  [c]
  (boolean? c))
(defn función-boolean?-2
  [c]
  (boolean? c))
(defn función-boolean?-3
  [c]
  (boolean? c))
(función-boolean?-1 true)
(función-boolean?-2 (== 2 2))
(función-boolean?-3 (+ 2 3))

(defn función-char?-1
  [c]
  (char? c))
(defn función-char?-2
  [c]
  (char? c))
(defn función-char?-3
  [c]
  (char? c))
(función-char?-1 \a)
(función-char?-2 (first [\@ 5 true]))
(función-char?-3 5)

(defn función-coll?-1
  [c]
  (coll? c))
(defn función-coll?-2
  [c]
  (coll? c))
(defn función-coll?-3
  [c]
  (coll? c))
(función-coll?-1 [1 9 1])
(función-coll?-2 '(true false))
(función-coll?-3 "Aloo")

(defn función-decimal?-1
  [n]
  (decimal? n))
(defn función-decimal?-2
  [n]
  (decimal? n))
(defn función-decimal?-3
  [n]
  (decimal? n))
(función-decimal?-1 1M)
(función-decimal?-2 565)
(función-decimal?-3 1.6M)

(defn función-float?-1
  [n]
  (float? n))
(defn función-float?-2
  [n]
  (float? n))
(defn función-float?-3
  [n]
  (float? n))
(función-float?-1 0.59)
(función-float?-2 5.121886103)
(función-float?-3 5)

(defn función-double?-1
  [n]
  (double? n))
(defn función-double?-2
  [n]
  (double? n))
(defn función-double?-3
  [n]
  (double? n))
(función-double?-1 0.59)
(función-double?-2 5.12)
(función-double?-3 5)

(defn función-ident?-1
  [n]
  (ident? n))
(defn función-ident?-2
  [n]
  (ident? n))
(defn función-ident?-3
  [n]
  (ident? n))
(función-ident?-1 :hola)
(función-ident?-2 \e)
(función-ident?-3 'abx)

(defn función-indexed?-1
  [n]
  (indexed? n))
(defn función-indexed?-2
  [n]
  (indexed? n))
(defn función-indexed?-3
  [n]
  (indexed? n))
(función-indexed?-1 [5 10 ])
(función-indexed?-2 #{:54 \b :7 \c})
(función-indexed?-3 '(0 4 90))

(defn función-int?-1
  [n]
  (int? n))
(defn función-int?-2
  [n]
  (int? n))
(defn función-int?-3
  [n]
  (int? n))
(función-int?-1 50)
(función-int?-2 13M)
(función-int?-3 -100)

(defn función-integer?-1
  [n]
  (integer? n))
(defn función-integer?-2
  [n]
  (integer? n))
(defn función-integer?-3
  [n]
  (integer? n))
(función-integer?-1 0) 
(función-integer?-2 [1 5 0])
(función-integer?-3 13N)

(defn función-keyword?-1
  [n]
  (keyword? n))
(defn función-keyword?-2
  [n]
  (keyword? n))
(defn función-keyword?-3
  [n]
  (keyword? n))
(función-keyword?-1 :a)
(función-keyword?-2 :v)
(función-keyword?-3 :XD)

(defn función-list?-1
  [n]
  (list? n))
(defn función-list?-2
  [n]
  (list? n))
(defn función-list?-3
  [n]
  (list? n))
(función-list?-1 (vector 80 90))
(función-list?-2 '([90 10] {:54 \b} #{10 20 30 40}))
(función-list?-3 '(8 true))

(defn función-map-entry?-1
  [n]
  (map-entry? n))
(defn función-map-entry?-2
  [n]
  (map-entry? n))
(defn función-map-entry?-3
  [n]
  (map-entry? n))
(función-map-entry?-1 (first {:a 1 :b 9}))
(función-map-entry?-2 '([90 10] {:54 \b} #{10 20 30 40}))
(función-map-entry?-3 (last {:v [10 20] :d '(10 20) }))

(defn función-map?-1
  [c]
  (map? c))
(defn función-map?-2
  [c]
  (map? c))
(defn función-map?-3
  [c]
  (map? c))
(función-map?-1 {:a 50 :b {:5 "H"}})
(función-map?-2 '([90 10] {:54 \b} #{10 20 30 40}))
(función-map?-3 (hash-map :951570 "Ale" :951580 "Maria"))

(defn función-nat-int?-1
  [n]
  (nat-int? n))
(defn función-nat-int?-2
  [n]
  (nat-int? n))
(defn función-nat-int?-3
  [n]
  (nat-int? n))
(función-nat-int?-1 0)
(función-nat-int?-2 90)
(función-nat-int?-3 -1)
;---------------------------------------Fin segundo commit-----------------------------------------------
;---------------------------------------Inicia Tercer Commit---------------------------------------------
(defn función-number?-1
  [n]
  (number? n))
(defn función-number?-2
  [n]
  (number? n))
(defn función-number?-3
  [n]
  (number? n))
(función-number?-1 0)
(función-number?-2 [90 10])
(función-number?-3 -50)

(defn función-pos-int?-1
  [n]
  (pos-int? n))
(defn función-pos-int?-2
  [n]
  (pos-int? n))
(defn función-pos-int?-3
  [n]
  (pos-int? n))
(función-pos-int?-1 0)
(función-pos-int?-2 -40)
(función-pos-int?-3 10)

(defn función-ratio?-1
  [n]
  (ratio? n))
(defn función-ratio?-2
  [n]
  (ratio? n))
(defn función-ratio?-3
  [n]
  (ratio? n))
(función-ratio?-1 0)
(función-ratio?-2 12/3)
(función-ratio?-3 -1/4)

(defn función-rational?-1
  [n]
  (rational? n))
(defn función-rational?-2
  [n]
  (rational? n))
(defn función-rational?-3
  [n]
  (rational? n))
(función-rational?-1 0.4)
(función-rational?-2 5)
(función-rational?-3 12/3)

(defn función-seq?-1
  [n]
  (seq? n))
(defn función-seq?-2
  [n]
  (seq? n))
(defn función-seq?-3
  [n]
  (seq? n))
(función-seq?-1 50)
(función-seq?-2 [80 90])
(función-seq?-3 (seq [50 9 10]))

(defn función-seqable?-1
  [n]
  (seqable? n))
(defn función-seqable?-2
  [n]
  (seqable? n))
(defn función-seqable?-3
  [n]
  (seqable? n))
(función-seqable?-1 "Aloo")
(función-seqable?-2 [8])
(función-seqable?-3 \a)
;--------------------------------------Fin tercer commit---------------------
;--------------------------------------Inicio Cuarto Commit------------------
(defn función-sequential?-1
  [n]
  (sequential? n))
(defn función-sequential?-2
  [n]
  (sequential? n))
(defn función-sequential?-3
  [n]
  (sequential? n))
(función-sequential?-1 (range 1 90))
(función-sequential?-2 [8 9 true])
(función-sequential?-3 55)

(defn función-set?-1
  [n]
  (set? n))
(defn función-set?-2
  [n]
  (set? n))
(defn función-set?-3
  [n]
  (set? n))
(función-set?-1 (hash-set 1 2 3))
(función-set?-2 (sorted-set \a \z \w))
(función-set?-3 {:55 \a :56 \b})

(defn función-some?-1
  [n]
  (some? n))
(defn función-some?-2
  [n]
  (some? n))
(defn función-some?-3
  [n]
  (some? n))
(función-some?-1 (hash-set 1 2 3))
(función-some?-2 nil)
(función-some?-3 "")
;----------------------------------------Fin cuarto commit-----------------------------------------
;---------------------------------------Inicia quinto commit---------------------------------------
(defn función-string?-1
  [n]
  (string? n))
(defn función-string?-2
  [n]
  (string? n))
(defn función-string?-3
  [n]
  (string? n))
(función-string?-1 '("Me ven"))
(función-string?-2 "Aloooo")
(función-string?-3 (first ["Me oyen"]))

(defn función-symbol?-1
  [n]
  (symbol? n))
(defn función-symbol?-2
  [n]
  (symbol? n))
(defn función-symbol?-3
  [n]
  (symbol? n))
(función-symbol?-1 \a)
(función-symbol?-2 'a)
(función-symbol?-3 (first [\@]))

(defn función-vector?-1
  [c]
  (vector? c))
(defn función-vector?-2
  [c]
  (vector? c))
(defn función-vector?-3
  [c]
  (vector? c))
(función-vector?-1 (hash-set 1 2 3))
(función-vector?-2 [true false 99 11 03])
(función-vector?-3 (vector 89 10))
;----------------------------------------Fin quinto commit-----------------------------------------
;---------------------------------------Inicia sexto commit---------------------------------------
;Funciones de orden superior
;orden superior
(defn función-drop-1
  [n c]
  (drop n c))

(defn función-drop-2
  [n c]
  (drop n c))

(defn función-drop-3
  [n c]
  (drop n c))

(función-drop-1 3 [10 90 4 6 6 5])
(función-drop-2 1 '(90 5))
(función-drop-3 0 {:b 8 :c 5})

(defn función-drop-last1
  [c]
  (drop-last c))
(defn función-drop-last2
  [n c]
  (drop-last n c))
(defn función-drop-last3
  [n c]
  (drop-last n c))

(función-drop-last1 [9 8 7 6 5 4])
(función-drop-last2 2 '(50 9 true false))
(función-drop-last3 2 [[5 10] [9 9] '(3 5 9 0)])

(defn función-drop-while-1
  [p c]
  (drop-while p c))
(defn función-drop-while-2
  [p c]
  (drop-while p c))
(defn función-drop-while-3
  [p c]
  (drop-while p c))
(función-drop-while-1 neg? [-70 8 8])
(función-drop-while-2 int? [1 0.2 0.4 0.6 0.8])
(función-drop-while-3 pos? [-8 -90 -90 5])

(defn función-every?-1
  [p c]
  (every? p c))
(defn función-every?-2
  [p c]
  (every? p c))
(defn función-every?-3
  [p c]
  (every? p c))
(función-every?-1 even? [-70 8 8])
(función-every?-2 even? '(-70 8 8))
(función-every?-3 pos? #{80 9 0 90})

(defn función-filterv-1
  [p c]
  (filterv p c))
(defn función-filterv-2
  [p c]
  (filterv p c))
(defn función-filterv-3
  [p c]
  (filterv p c))
(función-filterv-1 even? (range -50 -30))
(función-filterv-2 neg? [-80 6 -5 9 5])
(función-filterv-3 odd? #{80 9 0 90})

(defn función-group-by-1
  [p c]
  (group-by p c))
(defn función-group-by-2
  [p c]
  (group-by p c))
(defn función-group-by-3
  [p c]
  (group-by p c))
(función-group-by-1 odd? (range 10))
(función-group-by-2 count ["Aloo" "Estan" "Ahi"])
(función-group-by-3 int? #{8.5 9 5 4.3})

(defn función-iterate-1
  [p c]
  (iterate p c))
(defn función-iterate-2
  [p c]
  (group-by p c))
(defn función-iterate-3
  [p c]
  (group-by p c))
(función-iterate-1 inc 5)
(función-iterate-2 list? (range 5))
(función-iterate-3 indexed? [8 9 0])

(defn función-keep-1
  [p c]
  (keep p c))
(defn función-keep-2
  [p c]
  (keep p c))
(defn función-keep-3
  [p c]
  (keep p c))
(función-keep-1 odd? (range 500 505))
(función-keep-2 ratio? [true 5.5 0.333])
(función-keep-3 rational? '(1/4 5/30))
;-----------------------------------------Fin del sexto commit------------------------------
;-----------------------------------------Inicia septimo commit-----------------------------
(defn función-keep-indexed-1
  [c]
  (keep-indexed c))
(defn función-keep-indexed-2
  [p c]
  (keep-indexed p c))
(defn función-keep-indexed-3
  [p c]
  (keep-indexed p c))
(función-keep-indexed-1 #(if (odd? %1) %2))
(función-keep-indexed-2 #(if (pos? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-3 #(if (float? %2) %1) [5 9.2 5 7 11])

(defn función-map-indexed-1
  [p c]
  (map-indexed p c))
(defn función-map-indexed-2
  [p c]
  (map-indexed p c))
(defn función-map-indexed-3
  [p c]
  (map-indexed p c))
(función-map-indexed-1 vector "Aloo!")
(función-map-indexed-2 hash-map [5 9 0 5])
(función-map-indexed-3 list "Ansiedad")

(defn función-mapcat-1
  [p c]
  (mapcat p c))
(defn función-mapcat-2
  [p c]
  (mapcat p c))
(defn función-mapcat-3
  [p c]
  (mapcat p c))
(función-mapcat-1 reverse (vector "Alooo"))
(función-mapcat-2 reverse (vector (range 5 10)))
(función-mapcat-3 #(remove even? %) [[1 2] [2 2] [2 3]])

(defn función-mapv-1
  [p c]
  (mapv p c))
(defn función-mapv-2
  [p c d]
  (mapv p c d))
(defn función-mapv-3
  [p c d e]
  (mapv p c d e))
(función-mapv-1 dec [8 90 10 6])
(función-mapv-2 * [49 90 90] [85 5 0] )
(función-mapv-3 max [49 90 90] [85 5 0] [1 1 100])
;--------------------------------------Fin septimo Commit--------------------------------------------
;--------------------------------------Inicio Octavo Commit------------------------------------------

(defn función-merge-with-1
  [p c d]
  (merge-with p c d))
(defn función-merge-with-2
  [p c d e]
  (merge-with p c d e))
(defn función-merge-with-3
  [p c d e f]
  (merge-with p c d e f))
(función-merge-with-1 + {:a 1  :b 2} {:a 9  :b 98 :c 0})
(función-merge-with-2 into {"Hola" [17161210]} {"Aloo!" [16582840]}
                      {"Adios" [17161210]} )
(función-merge-with-3 merge {:x {:y {:a 1}}} {:x {:y {:b 2}}}
                      {:x {:y {:c 3}}} {:x {:y {:d 4}}})

(defn función-not-any?-1
  [p c ]
  (not-any? p c ))
(defn función-not-any?-2
  [p c ]
  (not-any? p c))
(defn función-not-any?-3
  [p c]
  (not-any? p c))
(función-not-any?-1 odd? '(2 4 6))
(función-not-any?-2 even? '(1 2 3))
(función-not-any?-3 nil? [true false nil])

(defn función-not-every?-1
  [p c]
  (not-every? p c ))
(defn función-not-every?-2
  [p c]
  (not-every? p c))
(defn función-not-every?-3
  [p c]
  (not-every? p c ))
(función-not-every?-1 odd? '(2 4 6))
(función-not-every?-2 even? '(1 2 3))
(función-not-every?-3 float? '(5.2 56.6))

(defn función-partition-by-1
  [p c]
  (partition-by p c))
(defn función-partition-by-2
  [p c ]
  (partition-by p c))
(defn función-partition-by-3
  [p c ]
  (partition-by p c))
(función-partition-by-1 #(= 2 %) [1 2 3 4 5 6])
(función-partition-by-2 identity "Aloooooooo!")
(función-partition-by-3 even? [8 9 07 9 4])

(defn función-remove-1
  [p c]
  (remove p c))
(defn función-remove-2
  [p c]
  (remove p c))
(defn función-remove-3
  [p c]
  (remove p c))
(función-remove-1 even? [5 9 4 6 1 0])
(función-remove-2 int? '(5 5.2 6.000 1M))
(función-remove-3 zero? [0 6 4 9 (mod 4 2)])
;-------------------------------------fin octavo commit----------------------------------
;--------------------------------------Inicio Noveno Commit------------------------------
(defn función-reverse-1
  [p  ]
  (reverse p))
(defn función-reverse-2
  [p ]
  (reverse p ))
(defn función-reverse-3
  [p]
  (reverse p ))
(función-reverse-1 "123456789")
(función-reverse-2 {6 9 7 9 4 3 0 4})
(función-reverse-3 ["r" "e" "c" "o" "n" "o" "c" "e" "r"])

(defn función-some-1
  [p c]
  (some p c))
(defn función-some-2
  [p c]
  (some p c))
(defn función-some-3
  [p c]
  (some p c))
(función-some-1 int? [5 0.8])
(función-some-2 double? [0.66666666 ])
(función-some-3 (and list? vector?) '('(5 89 9) [5 9]))

(defn función-sort-by-1
  [p c]
  (sort-by p c))
(defn función-sort-by-2
  [p c]
  (sort-by p c))
(defn función-sort-by-3
  [p c]
  (sort-by p c))
(función-sort-by-1 count ["aaa" "bb" "cc"])
(función-sort-by-2 first [[7 9] [5 4] [99 4]])
(función-sort-by-3 int? [ 6.4 5.4 7 9 10 11 ])

(defn función-split-with-1
  [p c]
  (split-with p c))
(defn función-split-with-2
  [p c]
  (split-with p c))
(defn función-split-with-3
  [p c]
  (split-with p c))
(función-split-with-1 (partial >= 3) [1 2 3 4 5])
(función-split-with-2 #{:c} [:a :b :c :d])
(función-split-with-3 int? [10 99 0.8 5 1 3])

(defn función-take-1
  [p]
  (take p))
(defn función-take-2
  [p c]
  (take p c))
(defn función-take-3
  [p c]
  (take p c))
(función-take-1 5)
(función-take-2 5 {1 2 3 4 5 6 7 8 9 0})
(función-take-3 3 '(1 2 3 4 5 6))
;-------------------------------------Fin Noveno Commit--------------
;-------------------------------------Inicio Decimo commit-----------
(defn función-take-last-1
  [p c]
  (take-last p c))
(defn función-take-last-2
  [p c]
  (take-last p c))
(defn función-take-last-3
  [p c]
  (take-last p c))
(función-take-last-1 5 "123456789")
(función-take-last-2 2 {6 9 7 9 4 3 0 4})
(función-take-last-3 7 ["r" "e" "c" "o" "n" "o" "c" "e" "r"])

(defn función-take-nth-1
  [p  ]
  (take-nth p))
(defn función-take-nth-2
  [p c ]
  (take-nth p c))
(defn función-take-nth-3
  [p c]
  (take-nth p c))
(función-take-nth-1 2)
(función-take-nth-2 2 (range 10))
(función-take-nth-3 2 ["r" "e" "c" "o" "n" "o" "c" "e" "r"]) 

(defn función-take-while-1
  [p c ]
  (take-while p c))
(defn función-take-while-2
  [p c]
  (take-while p c))
(defn función-take-while-3
  [p c]
  (take-while p c))
(función-take-while-1 neg? [-2 -1 0 1 2 3])
(función-take-while-2 (partial > 5.5) [6 9.5 7 9.3 4 3.1 0 4])
(función-take-while-3 int? [5 6.5 6 true false])

(defn función-update-1
  [p c a]
  (update p c a))
(defn función-update-2
  [p c a d e] 
  (update p c a d e))
(defn función-update-3
  [p c a d]
  (update p c a d))
(función-update-1 {:name "James" :age 26} :age inc)
(función-update-2 [1 2 3 4 5 6 7] 2 + 10 5)
(función-update-1 {:name "Jose" :age 10 :Peso 42.2} :Peso float?)

(defn función-update-in-1
  [p a c]
  (update-in p a c))
(defn función-update-in-2
  [p a b c]
  (update-in p a b c))
(defn función-update-in-3
  [p a c d b]
  (update-in p a c d b))
(función-update-in-1 {:name "James" :age 26} [:age] inc)
(función-update-in-2 {:name "Alejandra" :age 27 :altura 142 :peso 50} 
                     [:altura] + 20)
(función-update-in-3 {:a 3} [:a] / 3 20)

(defn función-reduce-kv-1
  [p c a]
  (reduce-kv p c a))
(defn función-reduce-kv-2
  [p c a]
  (reduce-kv p c a))
(defn función-reduce-kv-3
  [p c a]
  (reduce-kv p c a))
(función-reduce-kv-1 (fn [x y z] (assoc x y z)) 
                     {} ["Alexis" "Ramirez"])
(función-reduce-kv-2 (fn [x y z] (assoc x y z)) 
                     {} [5 9 10])
(función-reduce-kv-3 (fn [x y z] (assoc  x y z)) 
                     {:a "alpha" :b "yankee" :c "uniform" :d "delta" :e "alpha"} 
                     {:a "november" :b "oscar"})
;--------------------------------------Fin decimo Commit--------------------------------------------------------------
;Fin